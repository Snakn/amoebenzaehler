#! /usr/local/bin/python3

from collections import Counter
import tkinter as tk
from tkinter.filedialog import asksaveasfile 
from datetime import datetime

def save(): 
    files = [('Text Document', '*.txt'), ('All Files', '*.*')] 
    file = asksaveasfile(filetypes = files, defaultextension = files)
    file.writelines( f'{text.get(1.0, tk.END)}' )
    notification_text.set("File Saved")
    notification_label.config(fg="#0F0")
    file.close()

def clear_input():
  entry.delete(0, tk.END)


def clear_output():
  text.delete(1.0, tk.END)


def action():
  text.insert(tk.END, f'Neue Zählung: {datetime.now()}\n')
  eingabe = f'{entry.get()}'
  occurences = Counter( eingabe )
  notification_text.set("NOT SAVED")
  notification_label.config(fg="#F00")

  for num in occurences:
    text.insert(tk.END, f"{num}: {occurences[num]}\n")
    text.see(tk.END)


frame = tk.Tk()
frame.geometry("600x400")
frame.title("Amoebenzaehler 1.2")
#frame.attributes('-zoomed', True)

notification_text=tk.StringVar()
notification_text.set("")

#label = tk.Label(frame, text="Eingabe: ")
footer = tk.Frame(frame)
notification_label = tk.Label(footer,  textvariable=notification_text)


input_scroll_parent= tk.Label(frame)
inputscroll = tk.Scrollbar(input_scroll_parent, orient=tk.HORIZONTAL)

entry = tk.Entry(frame, font="TkDefaultFont 30", xscrollcommand=inputscroll.set)

# Buttons
buttons_parent = tk.Frame(input_scroll_parent)
button = tk.Button(buttons_parent, text="OK", command=action, font="TkDefaultFont, 15")
clear_button = tk.Button(buttons_parent, text="CLEAR INPUT",bg="#F00", command=clear_input, font="TkDefaultFont, 15")
lower_buttons_parent = tk.Frame(frame)
save_button = tk.Button(lower_buttons_parent, text="SAVE TO FILE",bg="#0F0" , font="TkDefaultFont, 15", command=lambda : save())
clear_output_button = tk.Button(lower_buttons_parent, text="CLEAR OUTPUT",bg="#F00", command=clear_output, font="TkDefaultFont, 15")


scroll = tk.Scrollbar(frame, orient=tk.VERTICAL)
text = tk.Text(frame, yscrollcommand=scroll.set)

# Pack GUI Elements
entry.pack(fill='x')
input_scroll_parent.pack(fill="x")
buttons_parent.pack(side="bottom", fill="x")
button.pack(side="left")
clear_button.pack(side="right")
footer.pack(side="bottom", fill="x")
lower_buttons_parent.pack(side="bottom", fill="x")
scroll.pack(side="right",fill="y",expand=False)
text.pack(side="left",fill="both",expand=True)
notification_label.pack(side="left")
save_button.pack(side="left")
clear_output_button.pack(side="right")

scroll.config(command=text.yview)

inputscroll.pack(fill="x", expand="False")
inputscroll.config(command=entry.xview)
tk.mainloop()