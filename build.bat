@echo off

:SETUP_VENV
echo Setting up virtual environment...
echo -------------------------------------
pip install virtualenv
virtualenv venv
call venv\scripts\activate.bat
pip install -r requirements.txt

echo Staring build...
echo -------------------------------------

::pyinstaller --onefile --noconsole amoebenzaehler.py --icon=app.ico
setup.py py2exe
echo Creating Zip...
echo -------------------------------------

set VERSION=1.2
set SZPATH="C:\Program Files\7-zip"
%SZPATH%\7z.exe a -tzip RELEASE\amoebenzaehler_v%VERSION%.zip .\dist

echo DONE
explorer .\RELEASE
pause
