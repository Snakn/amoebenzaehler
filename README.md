# Amoebenzaehler

This is a very simple tool that helps to manually count softagar colonies.
The user just has to enter a specific number or character for each cell type.

New features may be added.

Latest Release: [v1.2](https://gitlab.com/Snakn/amoebenzaehler/-/releases/v1.2) <br>
WINDOWS: [amoebenzaehler_v1.2.zip](/uploads/256740899de548b8ed72ac63e751a1f4/amoebenzaehler_v1.2.zip) <br>
WEB: [repl.it:amoebenzaehler1.1](https://repl.it/@Snaakn/Amoebenzaehler#main.py)
